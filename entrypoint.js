import meow from "meow";

import scripts from "#scripts/index";

// https://github.com/sindresorhus/meow
const cli = meow(
  `
		Commands
			$ ${Object.keys(scripts.ACTIONS)
        .map((el) => el.replace("_", ":"))
        .join(" | ")
        .toLowerCase()}

		Usage
			$ register
				-> Register commands to discords server
			
			$ start
				-> Start bot client

			$ restart
				-> Combine 'register' and 'start' commands
			
			$ db:normalize --input [-i] path/to/file.csv [--output [-o] path/to/output/file.csv]
				-> Take one CSV (from Sheet for exemple) and normalize it to fit the database
			
			$ db:dump --output [-o] path/to/output/file.csv
				-> Dump the database as CSV (to be used elsewhere if need be)
			
			$ db:populate --input [-i] path/to/file.csv
				-> Populate database from CSV file
			

		Examples
			$ register
			$ start
			$ db:normalize --input db/war.csv --output db/seeds/wars.csv
			$ db:dump --output db/seeds/dump.csv
			$ db:populate --input db/seeds/wars.csv
	`,
  {
    importMeta: import.meta,
    flags: {
      input: {
        type: "string",
        alias: "i",
      },
      output: {
        type: "string",
        alias: "o",
      },
    },
  }
);

scripts.dispatch(cli.input, cli.flags);
