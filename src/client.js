import { sequelize } from "#src/db";
import { CMD_NAMES, ENVIRONMENT_PROD } from "#src/constants";

export const ready = async () => {
  try {
    await sequelize.authenticate();
    await sequelize.sync();
    console.log("Connection with Database has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }

  console.log("Client is ready!");
};

export const interactionCreate = async (interaction, command) => {
  if (!interaction.isCommand() || !command) return;

  try {
    await command.execute(interaction);
  } catch (error) {
    console.error(error);
    await interaction.reply({
      content: "There was an error while executing this command!",
      ephemeral: true,
    });
  }
};

export const handlePermission = async (client) => {
  const clientCache = await client.guilds.fetch();

  if (clientCache) {
    let clientCommands = [];

    if (clientCache.size === undefined) {
      const commands = await clientCache.commands.fetch();
      clientCommands.push(...commands.values());
    } else {
      for (const cCache of clientCache.values()) {
        const cache = await cCache.fetch();
        const commands = await cache.commands.fetch();
        clientCommands.push(...commands.values());
      }
    }

    if (process.env.ENVIRONMENT === ENVIRONMENT_PROD)
      clientCommands = await client.application?.commands.fetch();
    const commands = clientCommands.filter((el) => el.name === CMD_NAMES.WIN);

    let permissions = [];
    if (process.env.ALLOWED_USERS) {
      const perm = await addUserPermissions();
      permissions.push(...perm);
    }

    if (commands.length > 0 && permissions.length > 0) {
      for (const command of commands) {
        await command.permissions.add({ permissions });
      }
    }
  }
};

const addUserPermissions = async () => {
  const allowedUsers = process.env.ALLOWED_USERS.split(",");

  if (!allowedUsers.length) return [];
  return allowedUsers.map((id) => ({
    id,
    type: "USER",
    permission: true,
  }));
};
