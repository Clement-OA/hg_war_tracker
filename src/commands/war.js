import { SlashCommandBuilder, codeBlock } from "@discordjs/builders";
import AsciiTable from "ascii-table";

import { WarModel } from "#src/db";
import { CMD_NAMES, CMD_DESCRIPTIONS_WAR } from "#src/constants";

/**
 * /war win 1050
 *    --> Get winner of given war
 * /war last
 *    --> Get last war winner
 * /war stats
 *    --> Get ratio and number of Win/Defeat of each faction
 * /war streak
 *    --> Get the best win streak of each faction
 */

export const data = new SlashCommandBuilder()
  .setName(CMD_NAMES.WAR)
  .setDescription(CMD_DESCRIPTIONS_WAR.WAR)
  .addSubcommand((subcommand) =>
    subcommand
      .setName(CMD_NAMES.WIN)
      .setDescription(CMD_DESCRIPTIONS_WAR.WIN)
      .addIntegerOption((option) =>
        option
          .setName(CMD_NAMES.NUMBER)
          .setDescription(CMD_DESCRIPTIONS_WAR.NUMBER)
          .setRequired(true)
      )
  )
  .addSubcommand((subcommand) =>
    subcommand.setName(CMD_NAMES.LAST).setDescription(CMD_DESCRIPTIONS_WAR.LAST)
  )
  .addSubcommand((subcommand) =>
    subcommand
      .setName(CMD_NAMES.STATS)
      .setDescription(CMD_DESCRIPTIONS_WAR.STATS)
  )
  .addSubcommand((subcommand) =>
    subcommand
      .setName(CMD_NAMES.STREAK)
      .setDescription(CMD_DESCRIPTIONS_WAR.STREAK)
  );

export const execute = async (interaction) => {
  const { options } = interaction;
  let message = "";
  let war;
  let table;

  const subcommand = options.getSubcommand();
  let number = options.getInteger("number");
  if (number === null) {
    const lastWar = await WarModel.max("number");
    number = lastWar;
  }

  switch (subcommand) {
    case "win":
      war = await WarModel.findOne({ where: { number } });
      if (war) message = `War ${number} was won by ${war.faction}`;
      else message = `War ${number} is not registered`;
      break;
    case "last":
      war = await WarModel.findOne({ where: { number } });
      message = `${war.faction} is the winner of the last war (${war.number})`;
      break;
    case "stats":
      const stats = await WarModel.count({ raw: true, group: ["faction"] });

      let total = 0;

      stats.forEach((el) => {
        total += el.count;
        el.faction = el.faction;
      });

      // TODO: Fix math rounding problem on percentage
      stats.forEach(
        (el) =>
          (el.percent = Number.parseFloat((el.count / total) * 100).toFixed(2))
      );

      table = new AsciiTable.factory({
        heading: ["Faction", "Victory", "Victory %", "Ranking"],
        rows: stats
          .sort((a, b) => b.count - a.count)
          .map((el, index) => [el.faction, el.count, el.percent, index + 1]),
      });

      message = codeBlock("md", table.render());
      break;
    case "streak":
      const data = await WarModel.findAll({ raw: true, order: ["number"] });
      let streak = { GE: 1, SU: 1, US: 1 };
      let currStreak = 1;
      data.forEach((el, i) => {
        if (i && data[i - 1].faction === el.faction) {
          currStreak += 1;
          if (currStreak > streak[el.faction]) streak[el.faction] = currStreak;
        } else currStreak = 1;
      });
      const rows = Object.entries(streak)
        .sort((a, b) => b[1] - a[1])
        .map((el, index) => [el[0], el[1], index + 1]);
      table = new AsciiTable.factory({
        heading: ["Faction", "Streak", "Ranking"],
        rows,
      });
      message = codeBlock("md", table.render());
      break;
    default:
      break;
  }
  await interaction.reply(message);
};

export default {
  data,
  execute,
};
