import { SlashCommandBuilder } from "@discordjs/builders";

import { WarModel } from "#src/db";
import { CMD_NAMES, CMD_DESCRIPTIONS_WIN } from "#src/constants";

/**
 * /win GE [1050]
 *    --> Set GE as the winner of current [or selected] war
 * /win SU [1050]
 *    --> Set SU as the winner of current [or selected] war
 * /win US [1050]
 *    --> Set US as the winner of current [or selected] war
 */

export const data = new SlashCommandBuilder()
  .setName(CMD_NAMES.WIN)
  .setDescription(CMD_DESCRIPTIONS_WIN.WIN)
  .setDefaultPermission(false)
  .addSubcommand((subcommand) =>
    subcommand
      .setName(CMD_NAMES.GE)
      .setDescription(CMD_DESCRIPTIONS_WIN.GE)
      .addIntegerOption((option) =>
        option
          .setName(CMD_NAMES.NUMBER)
          .setDescription(CMD_DESCRIPTIONS_WIN.NUMBER)
      )
  )
  .addSubcommand((subcommand) =>
    subcommand
      .setName(CMD_NAMES.SU)
      .setDescription(CMD_DESCRIPTIONS_WIN.SU)
      .addIntegerOption((option) =>
        option
          .setName(CMD_NAMES.NUMBER)
          .setDescription(CMD_DESCRIPTIONS_WIN.NUMBER)
      )
  )
  .addSubcommand((subcommand) =>
    subcommand
      .setName(CMD_NAMES.US)
      .setDescription(CMD_DESCRIPTIONS_WIN.US)
      .addIntegerOption((option) =>
        option
          .setName(CMD_NAMES.NUMBER)
          .setDescription(CMD_DESCRIPTIONS_WIN.NUMBER)
      )
  );

export const execute = async (interaction) => {
  const { options, user } = interaction;
  const userTag = user.tag;
  let shouldPing = false;

  let faction = options.getSubcommand();
  faction = faction.toUpperCase();

  // Get the war number from the options or increment it from the highest one in the DB
  let number = options.getInteger(CMD_NAMES.NUMBER);
  if (number === null) {
    const lastWar = await WarModel.max("number");
    number = lastWar + 1;
  }

  // Find the war to either update it or create it
  const war = await WarModel.findOne({ where: { number: number } });
  if (war) war.update({ faction, updatedBy: userTag });
  else {
    await WarModel.create({
      number,
      faction,
      createdBy: userTag,
      updatedBy: userTag,
    });
    shouldPing = true;
  }

  await interaction.reply(
    `${shouldPing ? "@here => " : ""}${faction} is the winner of war ${number}`
  );
};

export default {
  data,
  execute,
};
