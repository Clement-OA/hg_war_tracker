export const FACTION_SHORT_NAMES = ["GE", "SU", "US"];
export const ENVIRONMENT_PROD = 'PROD';

export const CMD_KEYS = {
  WAR: "WAR",
  WIN: "WIN",
  NUMBER: "NUMBER",
  LAST: "LAST",
  STATS: "STATS",
  STREAK: "STREAK",
  GE: "GE",
  SU: "SU",
  US: "US",
};

export const CMD_NAMES = {
  [CMD_KEYS.WAR]: "war",
  [CMD_KEYS.WIN]: "win",
  [CMD_KEYS.NUMBER]: "number",
  [CMD_KEYS.LAST]: "last",
  [CMD_KEYS.STATS]: "stats",
  [CMD_KEYS.STREAK]: "streak",
  [CMD_KEYS.GE]: "ge",
  [CMD_KEYS.SU]: "su",
  [CMD_KEYS.US]: "us",
};

export const CMD_DESCRIPTIONS_WIN = {
  [CMD_KEYS.WIN]: "Set the winner of (current) the war",
  [CMD_KEYS.GE]: "Set GE as the winner",
  [CMD_KEYS.SU]: "Set SU as the winner",
  [CMD_KEYS.US]: "Set US as the winner",
  [CMD_KEYS.NUMBER]: "number",
};

export const CMD_DESCRIPTIONS_WAR = {
  [CMD_KEYS.WAR]: "Get info about H&G wars!",
  [CMD_KEYS.WIN]: "Get winner of given war",
  [CMD_KEYS.NUMBER]: "Set war number",
  [CMD_KEYS.LAST]: "Get last war winner",
  [CMD_KEYS.STATS]: "Get ratio and number of Win/Defeat of each faction",
  [CMD_KEYS.STREAK]: "Get the best win streak of each faction",
};
