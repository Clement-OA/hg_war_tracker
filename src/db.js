import Sequelize from "sequelize";

import { FACTION_SHORT_NAMES, ENVIRONMENT_PROD } from "#src/constants";

const options = {
  logging: false,
  dialectOptions: {
    useUTC: true, // Read as UTC from database
  },
  timezone: "+00:00", // Write as UTC to database
};

export let sequelize;

if (process.env.ENVIRONMENT === ENVIRONMENT_PROD) {
  options.dialect = "postgres";
  options.dialectOptions.ssl = {
    require: true,
    rejectUnauthorized: false,
  };
  sequelize = new Sequelize(process.env.DATABASE_URL, options)
} else {
  options.dialect = "sqlite",
  options.storage = "./db/database.sqlite"; // SQLite only
  sequelize = new Sequelize(options)
}

export const WarModel = sequelize.define("war", {
  number: {
    type: Sequelize.INTEGER,
    allowNull: false,
    unique: true,
  },
  faction: {
    type: Sequelize.ENUM(),
    values: FACTION_SHORT_NAMES,
  },
  createdBy: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  updatedBy: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});

export default {
  sequelize,
  WarModel,
};
