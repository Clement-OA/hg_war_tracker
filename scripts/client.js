import { Client, Collection, Intents } from "discord.js";

import availableCommands from "#commands/index";
import { ready, interactionCreate, handlePermission } from "#src/client";

// Create a new client instance
const client = new Client({ intents: [Intents.FLAGS.GUILDS] });

client.commands = new Collection();

availableCommands.forEach((command) =>
  client.commands.set(command.data.name, command)
);

client.once("ready", async () => {
  await ready();

  // Set Avatar // TODO: Find a way to run it via a command, without restarting the Client!
  // https://discord.js.org/#/docs/discord.js/stable/class/ClientUser => Avatar/Username etc etc
  // client.user.setAvatar('./src/icon.png')
  await handlePermission(client);
});

client.on("interactionCreate", async (interaction) => {
  const command = client.commands.get(interaction.commandName);
  await interactionCreate(interaction, command);
});

export const startClient = () => {
  client.login(process.env.DISCORD_TOKEN);
};
