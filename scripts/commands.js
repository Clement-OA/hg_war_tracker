import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";

import availableCommands from "#commands/index";
import { ENVIRONMENT_PROD } from "#src/constants";

export const registerCommands = () => {
  const commands = availableCommands.map((command) => command.data.toJSON());

  const rest = new REST({ version: "9" }).setToken(process.env.DISCORD_TOKEN);

  let restRoutes;

  if (process.env.ENVIRONMENT !== ENVIRONMENT_PROD) {
    restRoutes = Routes.applicationGuildCommands(
      process.env.CLIENT_ID,
      process.env.GUILD_ID
    );
  } else {
    restRoutes = Routes.applicationCommands(process.env.CLIENT_ID);
  }

  rest
    .put(restRoutes, { body: commands })
    .then(() => console.log("Successfully registered application commands."))
    .catch(console.error);
};
