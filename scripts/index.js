import { startClient } from "#scripts/client";
import { registerCommands } from "#scripts/commands";
import { normalizeCSV, dumpAsCSV, populateFromCSV } from "#scripts/db";

export const ACTIONS = {
  REGISTER: "register",
  START: "start",
  RESTART: "restart",
  DB_NORMALIZE: "db:normalize",
  DB_POPULATE: "db:populate",
  DB_DUMP: "db:dump",
};

export const FLAGS = {};

export const FLAG_ALIAS = {};

export const dispatch = (action, flags) => {
  switch (action[0]) {
    case ACTIONS.REGISTER:
      registerCommands();
      break;
    case ACTIONS.START:
      startClient();
      break;
    case ACTIONS.RESTART:
      registerCommands();
      startClient();
      break;
    // DB Actions
    case ACTIONS.DB_NORMALIZE:
      normalizeCSV(flags.input, flags.output);
      break;
    case ACTIONS.DB_DUMP:
      dumpAsCSV(flags.output);
      break;
    case ACTIONS.DB_POPULATE:
      populateFromCSV(flags.input);
      break;
    default:
      console.log("Command does not exist");
      break;
  }
};

export default {
  ACTIONS,
  FLAGS,
  FLAG_ALIAS,
  dispatch,
};
