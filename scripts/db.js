import fs from "fs";
import path from "path";
import csv from "fast-csv";
import { WarModel } from "#src/db";

const NOW = new Date().toISOString(); // Date format => 'YYYY-MM-DD HH:mm:ss.SSS Z'
const INDEX_ROW_ID = 0;
const INDEX_ROW_NUMBER = 1;
const INDEX_ROW_FACTION = 2;
const INDEX_ROW_CREATED_BY = 3;
const INDEX_ROW_UPDATED_BY = 4;
const INDEX_ROW_CREATED_AT = 5;
const INDEX_ROW_UPDATED_AT = 6;
const DEFAULT_USER_TAG = "Bot#0000";
const HEADERS = [
  "id",
  "number",
  "faction",
  "createdBy",
  "updatedBy",
  "createdAt",
  "updatedAt",
];

class FileCSV {
  static write(filestream, rows, options) {
    return new Promise((res, rej) => {
      csv
        .writeToStream(filestream, rows, options)
        .on("error", (err) => rej(err))
        .on("finish", () => res());
    });
  }

  constructor(opts) {
    this.path = opts.path;
    this.writeOpts = { includeEndRowDelimiter: true, writeHeaders: false };
  }

  create(rows) {
    return FileCSV.write(fs.createWriteStream(this.path), rows, this.writeOpts);
  }

  append(rows) {
    return FileCSV.write(
      fs.createWriteStream(this.path, { flags: "a" }),
      rows,
      this.writeOpts
    );
  }
}

export const normalizeCSV = (input, output = "db/seeds/wars.csv") => {
  const newCSV = new FileCSV({ path: path.resolve(output) });

  csv
    .parseFile(`${input}`, { delimiter: ",", trim: true })
    .on("error", (error) => console.error(error))
    .on("data", (row) => {
      if (row[INDEX_ROW_NUMBER] && row[INDEX_ROW_FACTION]) {
        const createdBy = row[INDEX_ROW_CREATED_BY] || DEFAULT_USER_TAG;
        const updatedBy = row[INDEX_ROW_UPDATED_BY] || DEFAULT_USER_TAG;
        const createdAt = row[INDEX_ROW_CREATED_AT] || NOW;
        const updatedAt = row[INDEX_ROW_UPDATED_AT] || NOW;
        newCSV.append([
          {
            id: row[INDEX_ROW_ID],
            number: row[INDEX_ROW_NUMBER],
            faction: row[INDEX_ROW_FACTION],
            createdBy,
            updatedBy,
            createdAt,
            updatedAt,
          },
        ]);
      }
    })
    .on("end", (rowCount) => console.log(`Normalized ${rowCount} rows`));
};

export const dumpAsCSV = (output) => {
  const newCSV = new FileCSV({ path: path.resolve(output) });

  const findAll = () => {
    console.log("Dumping data...");
    return new Promise((resolve, reject) => {
      WarModel.findAll({ raw: true, order: ["number"] })
        .then((data) => resolve(data))
        .catch((error) => reject(error));
    });
  };

  const dump = async () => {
    const wars = await findAll();
    const headerRow = HEADERS.reduce(
      (acc, curr) => ({ ...acc, [curr]: curr }),
      {}
    );
    newCSV
      .create([headerRow])
      .then(() => newCSV.append(wars))
      .then(() => console.log(`Database dumped at ${output}`))
      .catch((error) => console.error(error));
  };

  dump();
};

export const populateFromCSV = (input) => {
  const readCSV = (path, options) => {
    return new Promise((resolve, reject) => {
      const rows = [];

      csv
        .parseFile(path, options)
        .on("error", (error) => reject(error))
        .on("data", (row) => {
          if (row.id === "") delete row.id;
          rows.push(row);
        })
        .on("end", () => resolve(rows));
    });
  };

  const insert = async () => {
    const rows = await readCSV(input, {
      headers: true,
      delimiter: ",",
      trim: true,
    });

    await WarModel.sync({ force: true }); // Drop & Create table
    await WarModel.bulkCreate(rows);
    console.log(`${rows.length} inserted`);
  };

  insert();
};
